<?php




/*

    Variable handling functions:
    gettype()
    var_dump()
    is_int
    is_string
    empty()
    isset()


    String handling functions;

    control structures: if/elseif/else/switch

    php operators


*/



// $number = 100;

// var_dump(is_string($number));

// echo gettype($number);

$number1='100';

var_dump(isset($number1));

$number2=null;
var_dump(empty($number2));


$number3 = "null";
var_dump(is_null($number3));

$number4 = [1,2,3,4];

echo "<pre>";
    print_r($number4);
echo "<pre>";



$number5 = [5,6,7,8,9,10];
print_r($number5);

// var_dump (serialize($number5));
$convertString = serialize($number5);
print_r($convertString);

$convertArray = unserialize($convertString);
print_r($convertArray);


$number6 = 500;

unset($number6);

echo $number6;



$number7 = "true";

if(is_int($number7)){
    echo "Number is Integer."; 
}
elseif(is_bool($number7)){
    echo "Number is Boolean.";
}
else{
    echo "Number not found.";
}



$day = date('D');
echo $day;


switch($day){
    case "Mon":
        echo "Today is Monday.";
        break;

    case "Tue":
        echo "Today is Tuesday.";
        break;
        
    case "Wed":
        echo "Today is Wednesday.";
        break;
        
    case "Fri":
        echo "Today is Holidayy.";
        break;
        
    default:
        echo "Not Found.";  
        break; 
}



$country_name = "Bangladesh";
echo md5($country_name);

echo strlen($country_name);

echo str_word_count($country_name);

echo strrev($country_name);

echo strpos($country_name, 'h');

echo str_replace('g', 'r', $country_name);







?>